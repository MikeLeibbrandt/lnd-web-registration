import React from "react";
import { withRouter } from "next/router";
import Footer from "@components/footer";
import Heading from "@components/heading";
import InputForm from "@components/input-form";
import PropTypes from "prop-types";

const Home = ({ siteKey }) => {
  return (
    <>
      <Heading />
      <InputForm sitekey={siteKey} />
      <Footer />
    </>
  );
};

Home.propTypes = {
  siteKey: PropTypes.string,
};
export default withRouter(Home);
