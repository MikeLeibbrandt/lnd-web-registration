import React from "react";
import Document, { Head, Main, NextScript } from "next/document";
import { ServerStyleSheet } from "styled-components";
import GoogleAnalytics from "next-simple-google-analytics";
import ReactFavic from "react-favic";

export default class MainDoc extends Document {
  static getInitialProps({ renderPage }) {
    const sheet = new ServerStyleSheet();
    const page = renderPage((App) => (props) =>
      sheet.collectStyles(<App {...props} />)
    );

    const styleTags = sheet.getStyleElement();

    return { ...page, styleTags };
  }
  render() {
    return (
      <html lang="en">
        <Head>
          <meta charSet="utf-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta content="width=device-width, initial-scale=1" name="viewport" />
          <meta
            name="keywords"
            content="Lesson Desk, E-learning Platform, Development, Group Training"
          />
          <meta
            name="description"
            content="largest e-learning platform in Africa, designed to train employees in a fun, digital and interactive format."
          />
          <meta name="author" content="Full Facing" />
          <ReactFavic
            name="Lesson Desk Registration"
            color="#797979"
            href="/static/images/meta"
          />

          <link
            href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&display=swap"
            rel="stylesheet"
          />

          <GoogleAnalytics id="UA-50091801-1" />

          {this.props.styleTags}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
