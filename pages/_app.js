import React from "react";
import App, { Container } from "next/app";
import { DefaultSeo } from "next-seo";

import "./_app.css";
import Reset from "@components/reset";

import { ThemeProvider } from "styled-components";
import PageNProgress from "next-styled-nprogress";

import theme from "@config/theme";

import "react-lazy-load-image-component/src/effects/blur.css";

import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();
class RegistrationPage extends App {
  componentDidMount() {
    if (window.navigator && navigator.serviceWorker) {
      navigator.serviceWorker.getRegistrations().then(function (registrations) {
        for (const registration of registrations) {
          registration.unregister();
        }
      });
    }
  }

  static async getInitialProps() {
    const reCaptchaDetails = {
      siteKey: publicRuntimeConfig.SITE_KEY,
    };
    return { reCaptchaDetails };
  }

  render() {
    const { Component, pageProps, reCaptchaDetails } = this.props;

    return (
      <Container>
        <DefaultSeo
          title="Lesson Desk"
          description="The largest E-learning Platform in Africa."
        />

        <Reset />

        <PageNProgress
          color={theme.nprogress}
          showSpinner={false}
          height="5px"
          delay={200}
        />

        <ThemeProvider theme={theme}>
          <Component siteKey={reCaptchaDetails.siteKey} {...pageProps} />
        </ThemeProvider>
      </Container>
    );
  }
}

export default RegistrationPage;
