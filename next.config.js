const path = require("path");

const withCSS = require("@zeit/next-css");
const withOptimizedImages = require("next-optimized-images");

module.exports = withOptimizedImages(
  withCSS({
    inlineImageLimit: -1,
    optimizeImagesInDev: false,
    mozjpeg: {
      quality: 95,
    },
    optipng: {
      optimizationLevel: 2,
    },
    gifsicle: {
      interlaced: true,
      optimizationLevel: 2,
    },
    webp: {
      preset: "default",
      quality: 95,
    },
    onDemandEntries: {
      maxInactiveAge: 25 * 1000,
      pagesBufferLength: 5,
    },
    publicRuntimeConfig: {
      SITE_KEY: process.env.RECAPTCHA_SITE_KEY,
    },
    webpack: (config) => {
      // Note: We set the path for styled-components to prevent duplicate instances
      config.resolve.alias["styled-components"] = path.resolve(
        "./node_modules",
        "styled-components"
      );

      config.resolve.alias["global/document"] = path.resolve(
        "./node_modules",
        "global",
        "document"
      );

      config.resolve.alias["global/window"] = path.resolve(
        "./node_modules",
        "global",
        "window"
      );

      config.resolve.alias["@components"] = path.join(__dirname, "components");
      config.resolve.alias["@config"] = path.join(__dirname, "config");
      config.resolve.alias["@utils"] = path.join(__dirname, "utils");
      config.resolve.alias["@sections"] = path.join(__dirname, "sections");
      config.resolve.alias["@global"] = path.join(__dirname, "global");
      config.resolve.alias["@styles"] = path.join(__dirname, "styles");
      config.resolve.alias["@images"] = path.join(
        __dirname,
        "assets",
        "images"
      );

      return config;
    },
  })
);
