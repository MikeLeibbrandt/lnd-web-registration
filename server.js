const path = require("path");

const express = require("express");
const next = require("next");
const helmet = require("helmet");
const morgan = require("morgan");
const compression = require("compression");
const nextLRUCache = require("next-lru-cache");

const submit = require("./controllers/submit");
const env = require("./config/env");
const app = next({ dev: env.isDev });

/**
 * Configurate global middleware
 * @param {Object} server Server instance
 */
function setMiddleware(server) {
  server.set("trust proxy", true); // https://stackoverflow.com/a/23426060/7027045
  server.use(helmet());
  server.use(morgan(env.isDev ? "dev" : "tiny"));
  server.use(express.urlencoded({ extended: false }));
  server.use(express.json());
  server.use(compression());
  server.use("/api/v1/submit", submit);
}

function setRoutes(server) {
  server.get("/v2/public/healthz", (req, res) => {
    res.json({ status: "Service is healthy." });
  });

  server.get("/service-worker.js", function (request, response) {
    response.sendFile(path.resolve(__dirname, ".next", "service-worker.js"));
  });
  nextLRUCache(server, app);
}

app
  .prepare()
  .then(() => {
    const server = express();
    setMiddleware(server);
    setRoutes(server);
    server.listen(env.port, (err) => {
      if (err) {
        throw err;
      }
      console.log(`> Running on port ${env.port}`);
    });
  })
  .catch((error) => {
    console.error(error.stack);
    process.exit(1);
  });
