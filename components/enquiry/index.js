import React from "react";
import { Content, Title, Text, Link } from "./styles";

const Enquiry = () => {
  return (
    <Content>
      <Title>Find out more about Lesson Desk.</Title>
      <Text>
        Lesson Desk enables you to guide employees through a comprehensive
        training journey, with each distinct phase addressing a key area of the
        recruitment and product training process.
      </Text>
      <Link align="center" href="https://www.lessondesk.com/">
        Visit our website
      </Link>
    </Content>
  );
};

export default Enquiry;
