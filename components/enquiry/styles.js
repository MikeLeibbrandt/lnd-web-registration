import styled from "styled-components";
import { radii, fontSizes, fontWeights, colors } from "@config/theme";
import { flexCenterColumn } from "@config/mixins";

export const Content = styled.div`
  ${flexCenterColumn};
  background: #fdfdfdfd;
`;

export const Title = styled.h1`
  color: ${colors.primary};
  font-weight: ${fontWeights.bold};
`;

export const Text = styled.p`
  color: ${colors.gray.xxdark};
  text-align: center;
  width: 38em;
`;

export const Link = styled.a`
  border-radius: ${radii.full};
  text-decoration: none;
  padding-top: 1.5em;
  height: 4em;
  width: 15em;
  text-transform: uppercase;
  margin: 1em 0 1em 0;
  border: none;
  background-color: ${colors.primary};
  box-shadow: 0 2px 44px 0 rgba(255, 164, 0, 0.19);
  color: ${colors.white};
  font-size: 1em;
  font-weight: ${fontWeights.bold};
  letter-spacing: 0.09px;
  line-height: 24px;
  text-align: center;
  cursor: pointer;

  @media (max-width: 1000px) {
    height: 2.2em;
    padding-bottom: 2em;
    font-size: ${fontSizes.xsmall};
    width: 16em;
  }
`;
