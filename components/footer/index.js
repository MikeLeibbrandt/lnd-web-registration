import {
  StyledFooter,
  FooterLogoContainer,
  FooterLogo,
  FooterLink,
  FooterLabel,
  FooterText,
} from "./styles";

const Footer = () => {
  return (
    <StyledFooter>
      <FooterLogoContainer>
        <FooterLogo />
        <FooterLink href="tel:+27215213850">
          <FooterLabel>Tel:</FooterLabel>
          <FooterText>+27 (21) 521 3850</FooterText>
        </FooterLink>
        <FooterLink href="fax:+27219445401">
          <FooterLabel>Fax:</FooterLabel>
          <FooterText>+27 (21) 944 5401</FooterText>
        </FooterLink>
        <FooterLink href="mailto:learn@lessondesk.com">
          <FooterLabel>Email:</FooterLabel>
          <FooterText>learn@lessondesk.com</FooterText>
        </FooterLink>
        <FooterText>
          2nd Floor, Fedgroup Place, Willie van Schoor Avenue, Bellville, 7530
        </FooterText>
      </FooterLogoContainer>
    </StyledFooter>
  );
};

export default Footer;
