import styled from "styled-components";

import theme, { fontWeights } from "@config/theme";

const { colors } = theme;

export const StyledFooter = styled.footer`
  position: relative;
  box-shadow: 0 48px 32px -8px rgba(0, 0, 0, 0.32);
  padding-top: 5em;
  z-index: 2;
  background: #fdfdfdfd;
`;

export const FooterLogoContainer = styled.div`
  width: 100%;
  height: 7em;
  background: ${colors.black};
  display: flex;
  align-items: center;
  justify-content: space-around;
  flex-wrap: wrap;

  @media (max-width: 900px) {
    padding: 0 20px 20px;
  }
`;

export const FooterLogo = styled.img.attrs({
  src: "/static/images/ld_logo_footer.svg",
  alt: "Lesson Desk Logo",
})`
  margin: 20px 0 30px 0;
  height: auto;
  width: 220px;

  @media (max-width: 900px) {
    margin-top: 40px;
    height: 30px;
    margin-bottom: 20px;
  }
`;

export const FooterLink = styled.a`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  color: #d1d2d2;
  font-size: 16px;
  letter-spacing: 0.07px;
  line-height: 32px;
  text-decoration: none;
  cursor: pointer;
  &:hover {
    color: ${colors.white};
  }
`;

export const FooterText = styled.div`
  color: #d1d2d2;
  font-size: 16px;
  letter-spacing: 0.07px;
  line-height: 32px;
`;

export const FooterLabel = styled.span`
  color: #d1d2d2;
  font-weight: ${fontWeights.extrabold};
  font-size: 16px;
  letter-spacing: 0.07px;
  line-height: 32px;
  font-weight: 600;
  margin-right: 10px;
`;
