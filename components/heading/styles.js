import styled from "styled-components";
import theme, { fontWeights, fontSizes } from "@config/theme";
import { flexCenterColumn, bgGraphic } from "@config/mixins";
const { colors } = theme;

export const Content = styled.div`
  ${flexCenterColumn};
  margin: auto;
  width: 70em;
  align-self: center;
  padding-top: 6em;
  @media (max-width: 600px) {
    padding-left: 0;
    margin: 0;
  }
`;

export const Logo = styled.img`
  width: 20em;
  height: 3em;
`;

export const LineGraphicRight = styled.div`
  ${bgGraphic};
  background-image: url("/static/images/page_element_lines.svg");
  background-position: left;
  background-size: cover;
  transform: rotate(180deg);
  left: 0;
  z-index: 0;
  height: 30vw;
  width: 21vw;
  top: 0;

  @media only screen and (max-width: 1100px) {
    display: none;
  }
`;

export const Title = styled.h1`
  font-weight: ${fontWeights.extrabold};
  color: ${colors.gray.xxdark};
  text-align: center;
  width: 18em;
  font-size: ${fontSizes.xxlarge};
`;

export const SubTitle = styled.p`
  text-align: center;
  color: ${colors.gray.xxdark};
  width: 45em;
  padding-top: 1.5em;
  font-size: ${fontSizes.xlarge};
`;
