import { Logo, Title, SubTitle, Content, LineGraphicRight } from "./styles";
const Heading = () => {
  return (
    <>
      <Content align="center">
        <LineGraphicRight />
        <Logo alt="" src="/static/images/ld_logo.svg" />
        <Title>Train your employees from the comfort of their homes.</Title>
        <SubTitle>
          Lesson Desk is offering all businesses a free subscription to our
          Individual Training app or web portal during the lockdown period. Get
          full access to our content Marketplace or upload your own training
          material.
        </SubTitle>
        <SubTitle>Stay safe. Stay home. Keep training.</SubTitle>
      </Content>
    </>
  );
};

export default Heading;
