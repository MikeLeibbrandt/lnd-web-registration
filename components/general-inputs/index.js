import TextInput from "@components/text-input";
import { QuestionContainer, Content, Button } from "./styles";
import ReCAPTCHA from "react-google-recaptcha";
import { useState } from "react";
import PropTypes from "prop-types";

const GeneralInputs = ({ onSubmit, sitekey }) => {
  const [reCaptcha, setReCaptcha] = useState(null);

  return (
    <Content>
      <QuestionContainer>
        <div>
          <TextInput required name="name" label="Name" />
          <TextInput
            required
            name="email"
            label="Email address"
            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
            placeholder="example@gmail.com"
          />
          <TextInput required name="contact" label="Contact number" />
        </div>
        <div>
          <TextInput required name="surname" label="Surname" />
          <TextInput required name="company" label="Company" />
        </div>
      </QuestionContainer>
      <ReCAPTCHA onChange={(value) => setReCaptcha(value)} sitekey={sitekey} />
      <Button onClick={() => onSubmit(reCaptcha)} value="button">
        Submit
      </Button>
    </Content>
  );
};

GeneralInputs.propTypes = {
  onSubmit: PropTypes.func,
  sitekey: PropTypes.string,
};
export default GeneralInputs;
