import styled from "styled-components";
import { colors, fontSizes, fontWeights, radii } from "@config/theme";

export const QuestionContainer = styled.div`
  display: flex;
  justify-content: space-around;
  padding: 0 16em;
  padding-bottom: 1em;
`;
export const Content = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (max-width: 1000px) {
    /* padding-left: 1em; */
  }
`;

export const Button = styled.button`
  border-radius: ${radii.full};
  height: 4em;
  width: 13em;
  text-transform: uppercase;
  margin: 3em 0 1em 0;
  border: none;
  background-color: ${colors.primary};
  /* padding-top: 3em; */
  box-shadow: 0 2px 44px 0 rgba(255, 164, 0, 0.19);
  color: ${colors.white};
  font-size: 1em;
  font-weight: ${fontWeights.bold};
  letter-spacing: 0.09px;
  line-height: 24px;
  text-align: center;
  cursor: pointer;

  @media (max-width: 1000px) {
    height: 2.2em;
    /* padding-bottom: 2em; */
    font-size: ${fontSizes.xsmall};
    width: 16em;
  }
`;
