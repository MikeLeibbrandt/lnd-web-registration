import { FormContext, useForm } from "react-hook-form";
import GeneralInputs from "@components/general-inputs";
import Enquiry from "@components/enquiry";
import { Form, Title, Text, Button, ModalContent, Logo } from "./styles";
import PropTypes from "prop-types";

import NProgress from "nprogress";
import toast from "just-toasty";
import InputSchema from "./schema";
import { useState } from "react";
import Modal from "react-modal";
const styleOverrides = {
  background: `url(${"/static/images/curve.svg"})`,
  backgroundRepeat: "no-repeat",
  backgroundPositionY: "25em",
  paddingBottom: "10em",
};

const InputForm = ({ sitekey }) => {
  const methods = useForm({});
  const { handleSubmit } = methods;
  const [showModal, setShowModal] = useState(false);

  const handleReCaptchaCheck = (reCaptcha) => {
    if (reCaptcha) {
      setShowModal(!showModal);
    } else {
      console.log("Does not exist!");
    }
  };

  const onSubmit = async (data) => {
    NProgress.start();
    try {
      await fetch("api/v1/submit", {
        method: "POST",
        cache: "no-cache",
        body: JSON.stringify({ data }),
        headers: {
          "Content-type": "application/json",
          Accept: "application/json",
        },
      });
    } catch (error) {
      NProgress.done();
      toast("Oops! There was an error submitting your application.");
    } finally {
      NProgress.done();
      setShowModal(!showModal);
    }
  };

  return (
    <>
      <FormContext {...methods}>
        <Form style={styleOverrides} validationSchema={InputSchema}>
          <GeneralInputs sitekey={sitekey} onSubmit={handleReCaptchaCheck} />
        </Form>
        <Enquiry />
        <Modal
          isOpen={showModal}
          onRequestClose={() => setShowModal(!showModal)}
          ariaHideApp={false}
          style={{
            overlay: {
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              zIndex: 100,
              backgroundColor: "rgba(0,0,0,0.7)",
            },
            content: {
              width: "55vw",
              left: "unset",
              right: "unset",
              // display: 'flex',
              // alignItems: 'center',
              // justifyContent: 'center',
            },
          }}
        >
          <Logo alt="" src="/static/images/ld_logo.svg" />
          <ModalContent>
            <Title>Thank you for your request.</Title>
            <Text>
              One of our team members will be in touch with you via email.
            </Text>
            <Button onClick={handleSubmit(onSubmit)} value="button">
              Close
            </Button>
          </ModalContent>
        </Modal>
      </FormContext>
    </>
  );
};

InputForm.propTypes = {
  sitekey: PropTypes.string,
};

export default InputForm;
