import styled from "styled-components";
import { colors, fontSizes, fontWeights, radii } from "@config/theme";
import { flexColumn, flexCenter, flexCenterColumn } from "@config/mixins";

export const Form = styled.div`
  ${flexColumn};
`;
export const Container = styled.div`
  ${flexCenter};
  flex-direction: column;
  text-align: center;
  flex-wrap: wrap;
  position: relative;
  overflow: hidden;
`;

export const Title = styled.h1`
  color: ${colors.primary};
  font-weight: ${fontWeights.bold};
`;

export const Text = styled.p`
  color: ${colors.gray.xxdark};
  text-align: center;
  width: 38em;
`;

export const Button = styled.button`
  border-radius: ${radii.full};
  height: 4em;
  width: 13em;
  text-transform: uppercase;
  margin: 3em 0 1em 0;
  border: none;
  background-color: ${colors.primary};
  /* padding-top: 3em; */
  box-shadow: 0 2px 44px 0 rgba(255, 164, 0, 0.19);
  color: ${colors.white};
  font-size: 1em;
  font-weight: ${fontWeights.bold};
  letter-spacing: 0.09px;
  line-height: 24px;
  text-align: center;
  cursor: pointer;

  @media (max-width: 1000px) {
    height: 2.2em;
    /* padding-bottom: 2em; */
    font-size: ${fontSizes.xsmall};
    width: 16em;
  }
`;

export const Logo = styled.img`
  width: 20em;
  height: 3em;
`;

export const ModalContent = styled.div`
  ${flexCenterColumn};
`;
