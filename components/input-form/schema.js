import * as yup from "yup";

export default yup.object().shape({
  name: yup.string().required("Required"),
  email: yup.string().required("Required"),
  surname: yup.string().required("Required"),
  contact: yup.string().required("Required"),
  company: yup.string().required("Required"),
});
