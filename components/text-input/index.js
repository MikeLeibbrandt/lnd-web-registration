import TextField from "@material-ui/core/TextField";
import PropTypes from "prop-types";
import {
  createMuiTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import { useFormContext } from "react-hook-form";
import theme from "@config/theme";

const { colors } = theme;

const useStyles = makeStyles(() => ({
  root: {
    "& label.MuiInputLabel-root": {
      width: "100%",
    },
    "& label.Mui-focused": {
      color: colors.primary,
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: colors.primary,
    },
  },
}));

function TextInputCustom({
  label,
  name,
  overrideStyle,
  pattern,
  required,
  placeholder,
}) {
  const classes = useStyles();
  const { register } = useFormContext();
  return (
    <TextField
      label={label}
      placeholder={placeholder}
      required={required}
      inputProps={{ style: overrideStyle, pattern }}
      inputRef={register}
      style={{ width: "35em", margin: "1em 4em" }}
      name={name}
      classes={{
        root: classes.root,
      }}
    />
  );
}

const lndTheme = createMuiTheme({});

TextInputCustom.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  overrideStyle: PropTypes.object,
  pattern: PropTypes.string,
  required: PropTypes.bool,
  placeholder: PropTypes.string,
};
export default function TextInput({
  label,
  name,
  overrideStyle,
  pattern,
  required,
  placeholder,
}) {
  return (
    <ThemeProvider theme={lndTheme}>
      <TextInputCustom
        label={label}
        name={name}
        required={required}
        pattern={pattern}
        placeholder={placeholder}
        overrideStyle={overrideStyle}
      />
    </ThemeProvider>
  );
}

TextInput.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  overrideStyle: PropTypes.object,
  pattern: PropTypes.string,
  required: PropTypes.bool,
  placeholder: PropTypes.string,
};
