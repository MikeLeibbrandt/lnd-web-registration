import { createGlobalStyle } from "styled-components";

import theme from "@config/theme";

const { fonts, colors } = theme;

const GlobalStyles = createGlobalStyle`
  html, body {
    height: 100%;
    font-family: ${fonts[0]};
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  * {
    box-sizing: border-box;
    padding: 0;
    outline: transparent;
  }

  body {
    background-color: ${colors.white};
  }

  main, #__next {
    height: 100%;
    scroll-behavior: smooth;
  }

  ul {
    list-style: none;
  }

  button {
    font-family: inherit;
  }

  .ReactModal__Overlay {
    opacity: 0;
    transition: opacity 300ms ease-in-out;
  }

  .ReactModal__Overlay--after-open{
      opacity: 1;
  }

  .ReactModal__Overlay--before-close{
      opacity: 0;
  }

  @keyframes fade-in {
    0% {
      opacity: 0;
    }

    100% {
      opacity: 1;
    }
  }

`;

export default GlobalStyles;
