import styled from "styled-components";

import theme from "@config/theme";
import { flexCenter } from "@config/mixins";

const { colors, fontWeights } = theme;

export const Content = styled.div`
  ${flexCenter};
  flex-direction: column;
  height: 100%;
  background-color: #47494b;
`;

export const Logo = styled.div`
  background-image: url("/static/images/logo.svg");
  height: 50px;
  width: 340px;
  max-width: 100%;
  max-width: 350px;
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;

  @media (max-width: 400px) {
    height: 40px;
    width: 280px;
  }
`;

export const NotFound = styled.div`
  background-image: url("/static/images/not-found.svg");
  height: 250px;
  width: 340px;

  max-width: 350px;
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
  margin: 2vw 1em;

  @media (max-width: 400px) {
    height: 200px;
    width: 280px;
  }
`;

export const Title = styled.h1`
  font-size: 1.6em;
  color: ${colors.white};
  text-align: center;
  max-width: 40em;
  margin: 0 1em 2em;
  font-weight: 400;
`;

export const Button = styled.button.attrs({
  type: "button",
})`
  font-size: 1.1em;
  color: #fff;
  padding: 0.6em 1em;
  border-radius: 0.3em;
  background-color: ${colors.primary};
  border: none;
  font-weight: ${fontWeights.bold};
  box-shadow: 0 0 8px 0 rgba(44, 44, 44, 0.5);
  cursor: pointer;
  margin-bottom: 3em;
`;
