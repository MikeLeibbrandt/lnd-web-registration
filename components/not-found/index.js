import React from "react";
import Link from "next/link";

import {
  Content,
  Logo,
  NotFound as NotFoundLogo,
  Title,
  Button,
} from "./styles";

const NotFound = () => (
  <Content>
    <Logo />
    <NotFoundLogo />
    <Title>{"Oops! The page you were looking for doesn't exist."}</Title>
    <Link href="/">
      <Button>Home</Button>
    </Link>
  </Content>
);

export default React.memo(NotFound);
