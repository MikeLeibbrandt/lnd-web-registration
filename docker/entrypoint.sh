#!/bin/bash
set -e

# Start Next.js app
cd /usr/src/app
echo "Starting next..."
npm start
