import { css } from 'styled-components'

export const flexBetween = css`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const flexAround = css`
  display: flex;
  justify-content: space-around;
  align-items: center;
`

export const flexCenter = css`
  display: flex;
  justify-content: center;
  align-items: center;
`

export const flexCenterRow = css`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`

export const flexCenterColumn = css`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const flexColumn = css`
  display: flex;
  flex-direction: column;
  align-items: start;
`

export const bgGraphic = css`
  position: absolute;
  background-position: center;
  background-size: contain;
  background-repeat: no-repeat;
`
