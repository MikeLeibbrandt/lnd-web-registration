require('dotenv').config()

const {
    NODE_ENV,
    PORT,
    BASIC_AUTH_UNAME,
    UPSTREAM_BASE_API_PORT,
    BASIC_AUTH_PWD,
    UPSTREAM_BASE_API_PROTOCOL,
    UPSTREAM_BASE_API_URL,
    GOOGLE_SHEET_ID,
    GOOGLE_SERVICE_ACCOUNT_EMAIL,
    GOOGLE_PRIVATE_KEY,
    RECAPTCHA_SITE_KEY
} = process.env

module.exports = {
    port: Number.parseInt(PORT, 10) || 3000,
    isDev: NODE_ENV !== 'production',
    google: {
        id: GOOGLE_SHEET_ID,
        serviceAccount: GOOGLE_SERVICE_ACCOUNT_EMAIL,
        pk: GOOGLE_PRIVATE_KEY
    },
    reCaptcha: { siteKey: RECAPTCHA_SITE_KEY },
    auth: {
        username: BASIC_AUTH_UNAME,
        password: BASIC_AUTH_PWD
    },
    baseURL: {
        protocol: UPSTREAM_BASE_API_PROTOCOL,
        url: UPSTREAM_BASE_API_URL,
        port: UPSTREAM_BASE_API_PORT
    }
}
