
const addAliases = (arr, aliases) =>
    aliases.forEach((key, i) =>
        Object.defineProperty(arr, key, {
            enumerable: false,
            get() {
                return this[i]
            },
        }),
    )

export const colors = {
    white: '#fff',
    gray: ['#f4f3f4', '#dfdfdf', '#d0d0d0', '#777879', '#808080', '#47494B'],
    black: '#212325',
    transparent: 'transparent',
    blue: ['#80dcff', '#4dceff', '#00abeb', '#0082b3', '#005d80'],
    green: '#37b049',
    red: ['#f6bdbb', '#ed7470', '#e53933', '#9f1814', '#71110e'],
    yellow: '#f8b31c',
    orange: ['#fff8eb', '#ffa400'],
}

colors.primary = colors.orange[1]
const orangeColorAliases = ['primaryLight', 'primary']
addAliases(colors.orange, orangeColorAliases)

const grayColorAliases = ['xlight', 'light', 'default', 'dark', 'xdark', 'xxdark']
addAliases(colors.gray, grayColorAliases)

export const fontWeights = [100, 200, 300, 400, 500, 600, 700]
const fontWeightsAliases = ['hairline', 'thin', 'light', 'medium', 'semi', 'bold', 'extrabold']
addAliases(fontWeights, fontWeightsAliases)

export const lineHeights = [1, 1.25, 1.375, 1.5, 1.625, 2]
const lineHeightsAliases = ['none', 'tight', 'snug', 'normal', 'relaxed', 'loose']
addAliases(lineHeights, lineHeightsAliases)

export const radii = [0, '4px', '16px', '24px', '30px', '9999px']
const radiiAliases = ['none', 'xsmall', 'small', 'large', 'xlarge', 'full']
addAliases(radii, radiiAliases)

export const fontSizes = [
    '0.5em',
    '0.625em',
    '0.75em',
    '14px',
    '16px',
    '18px',
    '1.4em',
    '3.5em',
]
const fontSizeAliases = [
    'xxxsmall',
    'xxsmall',
    'xsmall',
    'small',
    'medium',
    'large',
    'xlarge',
    'xxlarge',
]
addAliases(fontSizes, fontSizeAliases)

const fallbackFontStack = '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif'
export const fonts = ['Montserrat'].map(font => `${font}, ${fallbackFontStack}`)
const fontsAliases = ['Montserrat']
addAliases(fonts, fontsAliases)

export default {
    fontSizes,
    colors,
    fontWeights,
    lineHeights,
    radii,
    fonts,
    nprogress: '#F2A83B',
    headerHeight: '100px',
}
