const { GoogleSpreadsheet } = require('google-spreadsheet')
const express = require('express')
const router = express()
const env = require('../config/env')
const doc = new GoogleSpreadsheet(env.google.id)

router.post('/', async(req, res) => {
    const {name,
        email,
        contact,
        surname,
        company
    } = req.body.data
    
    try{
        await doc.useServiceAccountAuth({
            client_email: env.google.serviceAccount,
            private_key: env.google.pk.replace(/\\n/gm, '\n'),
        })
        await doc.loadInfo()

        const sheet = doc.sheetsByIndex[0]

        await sheet.addRow({name,
            email,
            contact,
            surname,
            company
        })
        return res.json({ success: true })
    } catch(error){ 
        return res.json({ success: false })
    }
})

module.exports = router
